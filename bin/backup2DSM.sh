#! /usr/bin/env bash
# usage :
#   $ backup2DSM.sh <DSM_user> <source_dir>
#
# 1.  For the NAS Synology administrator : activate SSH
# https://techarea.fr/tuto-ssh-cle-nas-synology/
# https://syskb.com/synchroniser-seedbox-nas-synology-rsync/
#
# 2.  On your laptop with user root you must configure borg
#     and Init the distant borg repo on NAS Synology
#   $ sudo su -l -c "backup2DSM.sh <my_dsm_user> init"

my_dsm_user=$1
my_dsm="cchaudier.myds.me"

# Source dir to backup
source_dir=/timeshift/
# Borg repo location on the NAS
dest_dir=/volume1/homes/${my_dsm_user}/backups/$(hostname)/timeshift

# SSH config
ssh_port="5022"
ssh_key="${HOME}/.ssh/id_rsa_mydsme"
ssh_opts="-oBatchMode=yes -i ${ssh_key} -p${ssh_port}"
ssh_connect_string="${my_dsm_user}@${my_dsm}"
ssh_string="${ssh_opts} ${ssh_connect_string}:${dest_dir}"

# BORG Settings
export BORG_REPO="${ssh_connect_string}:${dest_dir}"
export BORG_RSH="ssh ${ssh_opts}"
export BORG_REMOTE_PATH=/usr/local/bin/borg
backupname=$(date +%Y%m%d)

logfile="/var/log/backup2DSM.log"
exec &>> $logfile
chmod 666 $logfile

trap 'echo $(date) Backup interrupted >&2; exit 2 | tee $logfile' INT TERM

sync_with_dsm() {
  echo "_____________________________________________________________"
  echo "Sync local folder ${source_dir} to Synology DSM ${ssh_string} with name ::${backupname}"
  echo "$(date) Start backup"
  borg create \
    -v --stats --compression lzma,9 \
    --progress \
    ${BORG_REPO}::${backupname} $source_dir
  echo "$(date) End backup with success"

  echo "$(date) Rotating old backups"
  borg prune -v $BORG_REPO \
      --keep-daily=7 \
      --keep-weekly=4 \
      --keep-monthly=1
}

do_init() {
  which borg || apt-get install -y borgbackup
  test -f ${ssh_key} || init_sshkey # create sshkey only if is not exist
  ssh ${ssh_opts} ${ssh_connect_string} mkdir -p ${dest_dir}
  borg check $BORG_REPO \
    || ssh ${ssh_opts} ${ssh_connect_string} mkdir -p ${dest_dir} \
      && borg init --encryption=none $BORG_REPO
  crontab -l | grep -q "backup2DSM.sh $my_dsm_user" \
    || crontab < <(crontab -l ; echo "30 10 * * * /usr/local/bin/backup2DSM.sh $my_dsm_user")
  exit $?
}

init_sshkey() {
  echo "Create sshkey without passphrase !"
  ssh-keygen -b 4096 -C ${ssh_connect_string} -f ${ssh_key}
  echo "Copy sshkey on NAS Synology"
  ssh-copy-id -i ${ssh_key} -p 5022 ${ssh_connect_string}
}

if [[ $# -ne 1 ]]; then
  do_$2
fi

sync_with_dsm
